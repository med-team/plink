Source: plink
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>,
           Andreas Tille <tille@debian.org>,
           Charles Plessy <plessy@debian.org>,
           Dylan Aïssi <daissi@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               zlib1g-dev
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/med-team/plink
Vcs-Git: https://salsa.debian.org/med-team/plink.git
Homepage: http://zzz.bwh.harvard.edu/plink/
Rules-Requires-Root: no

Package: plink
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Recommends: med-config (>= 2.1)
Description: whole-genome association analysis toolset
 plink expects as input the data from SNP (single
 nucleotide polymorphism) chips of many individuals
 and their phenotypical description of a disease.
 It finds associations of single or pairs of DNA
 variations with a phenotype and can retrieve
 SNP annotation from an online source.
 .
 SNPs can evaluated individually or as pairs for their
 association with the disease phenotypes. The joint
 investigation of copy number variations is supported.
 A variety of statistical tests have been implemented.
 .
 Please note: The executable was renamed to plink1
 because of a name clash.  Please read more about this
 in /usr/share/doc/plink/README.Debian.
